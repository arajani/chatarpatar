var final_transcript = '';
var last10messages = []; //to be populated later
var roomNameIdMap = new Array();
var whisperTo = '';
var header = '';
var userName = '';
var socket;
var myRoomID;

var createRoomConstruct = /^[\/]create\s.{3,}/;
var joinRoomConstruct = /^[\/]join\s.{3,}/;
var leaveRoomConstruct = /^[\/]leave/;
var deleteRoomConstruct = /^[\/]delete\s.{3,}/;
var listRoomsConstruct = /^[\/](rooms){1}/;


/*
Functions
*/
function toggleNameForm() {
   $("#login-screen").toggle();
}

function toggleChatWindow() {
  $("#main-chat-screen").toggle();
}

// Pad n to specified size by prepending a zeros
function zeroPad(num, size) {
  var s = num + "";
  while (s.length < size)
    s = "0" + s;
  return s;
}

// Format the time specified in ms from 1970 into local HH:MM:SS
function timeFormat(msTime) {
  var d = new Date(msTime);
  return zeroPad(d.getHours(), 2) + ":" +
    zeroPad(d.getMinutes(), 2) + ":" +
    zeroPad(d.getSeconds(), 2) + " ";
}

function listRooms(){
  socket.emit("getRoomList");
}

function createRoom(roomName){
  var roomExists = false;
  socket.emit("check", roomName, function(data) {
      roomExists = data.result;
       if (roomExists) {
          $.notify("Room " + roomName + " already exists", "error");
        } else {      
        if (roomName.length > 0) { //also check for roomname
          socket.emit("createRoom", roomName);
          $("#errors").empty();
          $("#errors").hide();
          }
        }
    });
}

function joinRoom(roomName){
  socket.emit("joinRoom", roomNameIdMap[roomName]);
}

function leaveRoom(roomName){
  socket.emit("leaveRoom", myRoomID);
}

function deleteRoom(roomName){
  socket.emit("removeRoom", roomNameIdMap[roomName]);
}



$(document).ready(function() {
  //setup "global" variables first
  socket = io.connect("https://chatarpatar.herokuapp.com/");
  myRoomID = null;

  $("form").submit(function(event) {
    event.preventDefault();
  });

  $("#conversation").bind("DOMSubtreeModified",function() {
    $("#conversation").animate({
        scrollTop: $("#conversation")[0].scrollHeight
      });
  });

  $("#main-chat-screen").hide();
  $("#errors").hide();
  $("#name").focus();
  $("#join").attr('disabled', 'disabled'); 
  
  if ($("#name").val() === "") {
    $("#join").attr('disabled', 'disabled');
  }

  //enter screen
  $("#nameForm").submit(function() {
    var name = $("#name").val();
    userName = name;
    var device = "desktop";
    if (navigator.userAgent.match(/Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile/i)) {
      device = "mobile";
    }
    if (name === "" || name.length < 2) {
      $("#errors").empty();
      $("#errors").append("Please enter a name");
      $("#errors").show();
    } else {
      socket.emit("joinserver", name, device);
      toggleNameForm();
      toggleChatWindow();
      $("#msg").focus();
    }
  });

  $("#name").keypress(function(e){
    var name = $("#name").val();
    if(name.length < 2) {
      $("#join").attr('disabled', 'disabled'); 
    } else {
      $("#errors").empty();
      $("#errors").hide();
      $("#join").removeAttr('disabled');
    }
  });

  //main chat screen
  $("#chatForm").submit(function() {
    var msg = $("#msg").val();
    msg = $.trim(msg);

    if (msg.match(listRoomsConstruct)) {
      listRooms();
    } else if (msg.match(createRoomConstruct)) { 
      createRoom(msg.split(' ')[1]);
    } else if (msg.match(joinRoomConstruct)) {
      joinRoom(msg.split(' ')[1]);
    } else if (msg.match(leaveRoomConstruct)) {
      leaveRoom(msg.split(' ')[1]);
    } else if (msg.match(deleteRoomConstruct)) {
      deleteRoom(msg.split(' ')[1]);
    } else {
      socket.emit("send", new Date().getTime(), msg);      
    }

    $("#msg").val("");
  });

  $("#showCreateRoom").click(function() {
    $("#createRoomForm").toggle();
  });

  $("#createRoomBtn").click(function() {
    var roomExists = false;
    var roomName = $("#createRoomName").val();
    socket.emit("check", roomName, function(data) {
      roomExists = data.result;
       if (roomExists) {
          $("#errors").empty();
          $("#errors").show();
          $("#errors").append("Room <i>" + roomName + "</i> already exists");
        } else {      
        if (roomName.length > 0) { //also check for roomname
          socket.emit("createRoom", roomName);
          $("#errors").empty();
          $("#errors").hide();
          }
        }
    });
  });

  $("#msgs").on('click', '.joinRoomBtn', function() {
    var roomName = $(this).siblings("span").text();
    var roomID = $(this).attr("id");
    socket.emit("joinRoom", roomID);
  });

  $("#msgs").on('click', '.removeRoomBtn', function() {
    var roomName = $(this).siblings("span").text();
    var roomID = $(this).attr("id");
    socket.emit("removeRoom", roomID);
    $("#createRoom").show();
  }); 

  $("#leave").click(function() {
    var roomID = myRoomID;
    socket.emit("leaveRoom", roomID);
    $("#createRoom").show();
  });

//socket-y stuff
socket.on("exists", function(data) {
  $("#errors").empty();
  $("#errors").show();
  $("#errors").append(data.msg);
    toggleNameForm();
    toggleChatWindow();
});

socket.on("joined", function() {
  $("#errors").hide();
  socket.emit("createRoom", userName);
});

socket.on("history", function(data) {
  if (data.length !== 0) {
    $("#msgs").append("<li><strong><span class='text-warning'>Last 10 messages:</li>");
    $.each(data, function(data, msg) {
      $("#msgs").append("<li><span class='text-warning'>" + msg + "</span></li>");
    });
  } else {
    $("#msgs").append("<li><strong><span class='text-warning'>No past messages in this room.</li>");
  }
});

  socket.on("update", function(msg) {
    $("#msgs").append("<li>" + msg + "</li>");
  });

  socket.on("update-people", function(data){
    $("#people-header").empty();
    $("#people-header").append("<li class=\"btn btn-lg dropdown-toggle list-group-item active\" data-toggle=\"dropdown\"> People online <span class=\"badge\">" + data.count + "</span></li>");
    $("#people-header").append("<ul class=\"dropdown-menu\" role=\"menu\" id=\"people\"></ul>");
    $("#people").empty();
    $.each(data.people, function(a, obj) {
      $('#people').append("<li class=\"list-group-item\"><span>" + obj.name + "</span></li>");
    });
  });

  socket.on("chat", function(msTime, person, msg) {
    $("#msgs").append("<li><strong><span class='text-success'>" + timeFormat(msTime) + person.name + "</span></strong>: " + msg + "</li>");
  });

  socket.on("whisper", function(msTime, person, msg) {
    if (person.name === "You") {
      s = "whisper"
    } else {
      s = "whispers"
    }
    $("#msgs").append("<li><strong><span class='text-muted'>" + timeFormat(msTime) + person.name + "</span></strong> "+s+": " + msg + "</li>");
  });

  socket.on("roomList", function(data) {
    roomNameIdMap = new Array();
    $("#msgs").append("<li class=\"list-group-item active\">List of rooms <span class=\"badge\">"+data.count+"</span></li>");
     if (!jQuery.isEmptyObject(data.rooms)) { 
      $.each(data.rooms, function(id, room) {
        roomNameIdMap[room.name] = id;
        var html = "<button id="+id+" class='joinRoomBtn btn btn-default btn-xs' >Join</button>" + " " + "<button id="+id+" class='removeRoomBtn btn btn-default btn-xs'>Remove</button>";
        $('#msgs').append("<li id="+id+" class=\"list-group-item\"><span>" + room.name + "</span> " + html + "</li>");
      });
    } else {
      $("#msgs").append("<li class=\"list-group-item\">There are no rooms yet.</li>");
    }
  });

  socket.on("newRoomCreated", function(name, id){
    $.notify("New room created : " + name, "info");
    roomNameIdMap[name] = id;
  });

  socket.on("roomDeleted", function(name){
    $.notify("Room deleted : " + name, "info");
    delete roomNameIdMap[name]; 
  });

  socket.on("sendRoomID", function(data) {
    myRoomID = data.id;
  });

  socket.on("disconnect", function(){
    $("#msgs").append("<li><strong><span class='text-warning'>The server is not available</span></strong></li>");
    $("#msg").attr("disabled", "disabled");
    $("#send").attr("disabled", "disabled");
  });

});
